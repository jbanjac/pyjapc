PyJapc API
==========

Module pyjapc.pyjapc
--------------------

.. automodule:: pyjapc.pyjapc
    :members:
    :undoc-members:
    :show-inheritance:

Module pyjapc.rbac_dialog
-------------------------

.. automodule:: pyjapc.rbac_dialog
    :members:
    :undoc-members:
    :show-inheritance:
