
Let's do a live-plot of a simple parameter
==========================================

Subscribe to a simple parameter and plot it over time with live-updates

.. code:: python

    %pylab tk


.. parsed-literal::

    Populating the interactive namespace from numpy and matplotlib


.. code:: python

    from pyjapc import *

.. code:: python

    p = PyJapc( noSet=True )

.. code:: python

    valueBuffer = list()    #We will use a list to buffer the values for plotting
                            #Compared to arrays, lists are slow but easily extendable

Create the initial plot, which will be updated with new data in the next step
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This will create an empty plot window with no data yet. Please keep it
open for the next steps

.. code:: python

    line, = plot( 0, "-o" )
    axis((0, 50, 0.9, 1.1))
    xlabel("Sample no.")
    ylabel("Current [{0}]".format( p.getParam("CB.BHB1100/Acquisition#current_unit") ) )




.. parsed-literal::

    <matplotlib.text.Text at 0x7f8039794240>



Define a simple callback function, which will update the plot when new data arrives
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code:: python

    def myValueCallback( parameterName, newValue ):
        global valueBuffer, line
        valueBuffer.append( newValue ) #Just append the new data to the buffer
        line.set_data( range(len(valueBuffer)) ,valueBuffer )
        line.figure.canvas.draw()

Alternatively: A more fancy callback could implement a rolling view like this
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code:: python

    def myValueCallback( parameterName, newValue ):
        global valueBuffer, line
        valueBuffer.append( newValue ) # append the new data to the buffer
        if len( valueBuffer ) > 30:    # Keep max. 30 points in the buffer
            valueBuffer.pop(0)         # Remove the oldest point
        line.set_data( range(len(valueBuffer)) ,valueBuffer )
        line.figure.canvas.draw()

Start the subscription
~~~~~~~~~~~~~~~~~~~~~~

.. code:: python

    valueBuffer.clear()
    p.subscribeParam( "CB.BHB1100/Acquisition#currentAverage", myValueCallback )
    p.startSubscriptions()

.. code:: python

    display(gcf())



.. image:: TimeSeries_files/TimeSeries_15_0.png


.. code:: python

    p.stopSubscriptions()

