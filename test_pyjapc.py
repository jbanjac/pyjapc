# -*- coding: utf-8 -*-
"""
Created on Wed May 20 08:59:32 2015
 Testfile for pytest
 Start with py.test command
@author: mbetz
"""
import pytest, time
from numpy import *
from pyjapc import *

@pytest.fixture(scope="session")
def my_fixture():
    global japc
    japc = PyJapc( incaAcceleratorName="LHC", noSet=True )
    japc.rbacLogin()

pytestmark = pytest.mark.usefixtures('my_fixture')

def convertHelper( dtype ):
    """ Take a random value of type dtype,
        convert into Java World and back.
        Check if value changed
    """
    x = dtype( random.random()*4200 )
    y = japc._convertValToPy( japc._convertPyToVal( x ) )
    assert( x == y )

def test_convert_py_to_simple():
    assert( japc._convertPyToSimpleVal(1).toString() == '(int:1) -> 1')
    assert( japc._convertPyToSimpleVal(1.1).toString() == '(double:1) -> 1.1' )
    assert( japc._convertPyToSimpleVal(True).toString() == '(boolean:1) -> true' )
    assert( japc._convertPyToSimpleVal(False).toString() == '(boolean:1) -> false' )
    assert( japc._convertPyToSimpleVal("HelloTesti").toString() == '(String:1) -> HelloTesti' )

    a = array(["hello","world", "TestiUltraLongBlaString"])
    ja = japc._convertPyToSimpleVal(a)
    res = japc._convertSimpleValToPy( ja )
    assert( res[0] == "hello" )
    assert( res[1] == "world" )
    assert( res[2] == "TestiUltraLongBlaString" )
    assert( res.size == 3 )
    sVal = japc._convertPyToVal({"a":4, "b":ones((2,2)), "c":"Wow", "d":True, "e":False})
    #assert( sVal.toString() == 'd (boolean:1) -> true\ne (boolean:1) -> false\nb (double[][]:2x2) -> [1.0, 1.0], [1.0, 1.0]\nc (String:1) -> Wow\na (int:1) -> 4\n' )
    #(Order is not always the same)
    a = ones(4, dtype=bool)
    a[2] = False
    assert( japc._convertPyToSimpleVal(a).toString() == '(boolean[]:4) -> true, true, false, true' )
    # Systematically check all Java types
    convertHelper(int16)
    convertHelper(int32)
    convertHelper(int64)
    convertHelper(float32)
    convertHelper(float64)
    convertHelper(double)
    convertHelper(bool)
    convertHelper(int)
    convertHelper(float)
    convertHelper(str)

def test_array_conversions():
    numpyArr = arange(4,dtype=double).reshape(2,2)
    twoDParamValue = japc._convertPyToSimpleVal( numpyArr )
    #assert( type( twoDParamValue ) == jpype._jclass.cern.japc.spi.value.simple.DoubleArrayValue )
    reconvertedNumpyArr = japc._convertSimpleValToPy( twoDParamValue )
    assert( array_equal( numpyArr, reconvertedNumpyArr ) == True )

    #Test an empty array (nasty!)
    numpyArr = arange(0,dtype=double)
    twoDParamValue = japc._convertPyToSimpleVal( numpyArr )
    #assert( type( twoDParamValue ) == jpype._jclass.cern.japc.spi.value.simple.DoubleArrayValue )
    reconvertedNumpyArr = japc._convertSimpleValToPy( twoDParamValue )
    assert( array_equal( numpyArr, reconvertedNumpyArr ) == True )

def test_parameter_groups():
    p1 = "LHC.BQTrig.HB1/Acquisition#sequence"
    p2 = "LHC.BQTrig.HB2/Acquisition#sequence"
    gr = japc._getJapcPar([p1,p2])
    assert( isinstance( gr, jp.JClass("cern.japc.spi.group.ParameterGroupImpl") ) )
    gr = japc._getJapcPar([p1])
    assert( isinstance( gr, jp.JClass("cern.japc.spi.group.ParameterGroupImpl") ) )
    pa = japc._getJapcPar( p1 )
    assert( isinstance( pa, jp.JClass("cern.japc.spi.adaptation.FieldFilteringParameterAdapter") ) )


def test_selector_business():
    assert( japc._selector.toString() == "LHC.USER.ALL" )
    assert( japc._selector.dataFilter == None )
    assert( japc._selector.isOnChange() == 1 )
    japc.setSelector("ALL", False)
    assert( japc._selector.toString() == "ALL" )
    assert( japc._selector.dataFilter == None )
    assert( japc._selector.isOnChange() == 0 )
    japc.setSelector("LHC.USER.ALL", True, {"averaging":1} )
    assert( japc._selector.toString() == "LHC.USER.ALL" )
    assert( japc._selector.dataFilter.toString() == "averaging (int:1) -> 1\n" )
    assert( japc._selector.isOnChange() == 1 )
    assert( type(japc.getParam( "LHC.BQS.SCTL/SetElectronics#B2_HOR_JFW_ATTEN", dataFilterOverride=None )) == int )
    japc.setSelector("LHC.USER.ALL", True, None )
    assert( type(japc.getParam( "LHC.BQS.SCTL/SetElectronics#B2_HOR_JFW_ATTEN" )) == int )

def test_getParam():
    # Simple values
    d = japc.getParam( "LHC.BQS.SCTL/SetElectronics" )
    assert( type(d) == dict )
    assert( len(d) > 0 )
    # Simple arrays
    d = japc.getParam( "LHC.BQS.SCTL/BunchSelector#BunchSel2Slots" )
    assert( type(d) == ndarray )
    assert( len(d) == 3564 )
    # 2D array (needs login!)
    d = japc.getParam("LHC.BQBBQ.UA47.BQS_B1/QFitterSetting#useCaseFittingParameters")
    assert( type(d) == ndarray )
    assert( d.ndim == 2 )
    
def test_getEnum():
    parName = "PA.TFB-DSPU-V-NEW/OperCtrl#control1PickUp02select"
    en = japc.getParam( parName, timingSelectorOverride="CPS.USER.SFTPRO1" )
    assert( type(en[0]) == int )
    assert( type(en[1]) == str )
    
def test_setEnum():
    parName = "PA.TFB-DSPU-V-NEW/OperCtrl#control1PickUp02select"
    japc.setParam( parName, 0, timingSelectorOverride="CPS.USER.SFTPRO1" )
    japc.setParam( parName, "pickUp02select", timingSelectorOverride="CPS.USER.SFTPRO1" )    

def test_getParamHeader():
    # Header info
    val, head = japc.getParam( "BLRSPS_LSS2/Acquisition#calLosses", timingSelectorOverride="SPS.USER.SFTPRO2", getHeader=True )
    assert( type(val) == ndarray )
    assert( type(head) == dict )
    assert( len( head ) > 0 )

def test_getParams():
    # Test getting a parameter group without header
    pars = [ "LHC.BQS.SCTL/BunchSelector#BunchSel2Slots", "LHC.BQS.SCTL/SetElectronics" ]
    vals = japc.getParam( pars, timingSelectorOverride="LHC.USER.ALL" )
    assert( type(vals) == list )
    assert(  len(vals) == 2 )
    assert( type(vals[0]) == ndarray )
    # Test getting a parameter group with headers
    vals, heads = japc.getParam( pars, timingSelectorOverride="LHC.USER.ALL", getHeader=True )
    assert( type(vals) == list )
    assert(  len(vals) == 2 )
    assert( type(vals[0]) == ndarray )
    assert( type(heads) == list )
    assert(  len(heads) == 2 )
    assert( type(heads[0]) == dict )
    assert( len( heads[0] ) > 0 )

def test_set():
    # Test simulated SETs of correct dimension
    japc.setParam( "LHC.BQS.SCTL/SetElectronics#B1_VER_BASEBAND_GAIN", 42 )
    aaa = japc.getParam( "LHC.BQS.SCTL/BunchSelector#BunchSel2Slots" )
    aaa[2] = 42
    japc.setParam( "LHC.BQS.SCTL/BunchSelector#BunchSel2Slots", aaa )
    japc.setParam( "LHC.BQS.SCTL/BunchSelector#BunchSel2Slots", ones((1,3564)) )
    japc.setParam( "LHC.BQS.SCTL/BunchSelector#BunchSel2Slots", ones(3564) )
    # Test simulated SETs of wrong dimension
    with pytest.raises( TypeError ):
        japc.setParam( "LHC.BQS.SCTL/BunchSelector#BunchSel2Slots", ones(3563) )

def test_subscribing():
    parmName = "LHC.BQS.SCTL/BunchSelector#BunchSel2Slots"
    def cb1( parName, val):
        global pN, v
        pN = parName
        v = val
    japc.subscribeParam( parmName, cb1 )
    japc.startSubscriptions()
    time.sleep(0.1)
    japc.clearSubscriptions()
    assert( pN == parmName )
    assert( type(v) == ndarray )
    assert( len(v) > 0 )

def test_subscribing_header():
    parmName = "LHC.BQS.SCTL/BunchSelector#BunchSel2Slots"
    def cb1( parName, val, header):
        global pN, v, hh
        pN = parName
        v = val
        hh = header
    japc.subscribeParam( parmName, cb1, getHeader=True )
    japc.startSubscriptions()
    time.sleep(0.1)
    japc.clearSubscriptions()
    assert( pN == parmName )
    assert( type(v) == ndarray )
    assert( len(v) > 0 )
    assert( type(hh) == dict )
    assert( len(hh) > 0 )

def test_subscribing_groups_headers():
    parmName = "LHC.BQS.SCTL/BunchSelector#BunchSel2Slots"
    def cb1( parNames, vals, headers ):
        global pns, vs, hhs
        pns = parNames
        vs = vals
        hhs = headers
    japc.subscribeParam( [parmName,parmName], cb1, getHeader=True )
    japc.startSubscriptions()
    time.sleep(0.1)
    japc.clearSubscriptions()
    assert( pns == [parmName,parmName] )
    assert( type(vs) == list )
    assert( type(vs[0]) == ndarray )
    assert( len(vs[0]) > 0 )
    assert( type(hhs) == list )
    assert( type(hhs[0]) == dict )
    assert( len(hhs[0]) > 0 )

def test_subscribing_groups():
    parmName = "LHC.BQS.SCTL/BunchSelector#BunchSel2Slots"
    def cb1( parNames, vals ):
        global pns, vs
        pns = parNames
        vs = vals
    japc.subscribeParam( [parmName,parmName], cb1 )
    japc.startSubscriptions()
    time.sleep(0.1)
    japc.clearSubscriptions()
    assert( pns == [parmName,parmName] )
    assert( type(vs) == list )
    assert( type(vs[0]) == ndarray )
    assert( len(vs[0]) > 0 )

def test_set_maps():
    # Full set
    p = japc.getParam("LHC.BQS.SCTL/SetElectronics")
    p['PREAMP_RELAYS_OK'] = [41]
    p['B1_HOR_CALSWITCH1'] = [42]
    japc.setParam("LHC.BQS.SCTL/SetElectronics", p)
    # Partial set
    p = japc.getParam("LHC.BQS.SCTL/SetElectronics")
    p.pop("B2_VER_COMPPATH")
    japc.setParam("LHC.BQS.SCTL/SetElectronics", p)
    # Wrong dimension should give an excpetion
    p = japc.getParam("LHC.BQS.SCTL/SetElectronics")
    p['B1_HOR_CALSWITCH1'] = [42,43]
    with pytest.raises( TypeError ):
        japc.setParam("LHC.BQS.SCTL/SetElectronics", p)
    with pytest.raises( AttributeError ):
        japc.setParam("LHC.BQS.SCTL/SetElectronics", 0)
    with pytest.raises( NameError ):
        japc.setParam("LHC.BQS.SCTL/SetElectronics", {"xxxB2_VER_B1_HOR_PREAMP_SW56":0})

def test_get_discreteFunctions():
    # Get discreteFunction
    dfv = japc.getParam("rmi://lsa/SPSBEAM/B", timingSelectorOverride="SPS.USER.SFTION4" )
    assert( type(dfv) == ndarray )
    assert( dfv.ndim == 2 )
    # Get discreteFunctionList  (   Commented out because the server is down :(   )
    #dfl = japc.getParam("BA3.FGBEAMPOFF/Settings#yData", timingSelectorOverride="PSB.USER.MD3")
    #assert( type(dfl) == list )
    #assert( dfl[0].ndim == 2 )
    # Set discreteFunctionList
    #testDat = [ array([[0,2],[3,4]]), array([[0,6,7],[4,9,1e-12]]) ]
    #japc.setParam( "INCA.TESTDEV-01-001-F3/FunctionControl#amplitudes", testDat, timingSelectorOverride="PSB.USER.ZERO" )
    #getDat = japc.getParam( "INCA.TESTDEV-01-001-F3/FunctionControl#amplitudes", timingSelectorOverride="PSB.USER.ZERO" )
    #assert( array_equal(testDat, getDat) == True )   #Need noSet=False for this one ...

#def test_get_enum():
# Not yet implemented
#    enum = japc.getParam("BR4.QCHZ-VT-INCA/Status#value")
#    assert( type(enum) == dict )
#    assert( type(enum["string"]) == str )
#    assert( type(enum["code"]) == int )
